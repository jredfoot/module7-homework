	#!/usr/bin/bash
	library="./words.txt"
empty="\."
games=0

if [ ! -r "$library" ]; then 
echo "$0 missing a library of words $library" >&2
echo "(online: https://intuitivestories.com/wicked/examples/long-words.txt" >&2
echo "Save the file as $library and you're ready to play Hangman!)" >&2
exit 1
fi

while [ "$guess" != "quit" ]; do
match="$(shuf -n 1 words.txt)"
	#echo $match
	if [ $games -gt 0 ] ; then
	echo ""
	echo "*** NEW GAME ***"
	fi
	games="$(( $games + 1 ))"
	guessed=""; guess=""; bad=${1:-6}
	partial="$(echo $match | sed "s/[^$empty${guessed}]/-/g")"
	
	while [ "$guess" != "$match" -a "$guess" != "quit" ] ; do
	echo ""
	
		if [ ! -z "$guessed" ]; then 
		echo -n "guessed: $guessed"
		fi
	echo "steps from gallows: $bad, word so far: $partial"
	echo -n "Guess a Letter: "
	read guess
	echo ""
	if [ "$guess" == "$match" ] ; then 
	echo "You got it!"
	elif [ "$guess" = "quit" ] ; then 
	exit 0
	elif [ $(echo $guess | wc -c | sed 's/[^[:digit:]]//g') -ne 2]; then
	echo "You can only guess one letter at a time."
	elif [ ! -z "$(echo $guess | sed 's/[[:lower:]]//g')" ]; then
	echo "Please only use lowercase letters in your guesses."
	elif [ -z "$(echo $guess | sed "s/[$empty$guessed]//g")" ]; then
	echo "You have already tried the letter $guess"
	elif [ "$(echo $match | sed "s/$guess/-/g")" != $match ]; then
	guessed=$guessed$guess
	partial="$(echo $match | sed "s/[^$empty${guessed}]/-/g")"
		if [ "$partial" = "$match" ] ; then
		echo "** You have won your freedom! The word was \"$match\"."
		guess="$match"
		else
		echo "The letter \"$guess\" appears in the word!"
		fi
	elif [ $bad -eq 1 ]; then
	echo "You have run out of guesses and lost your chance at freedom. Prepare to hang..."
	echo "The word you were trying to match was \"$match\"."
	guess=$match
	else
	echo "\"$guess\" does not appear in the word. Sorry!"
	guessed=$guessed$guess
	bad=$(( $bad - 1))
	fi
	done
done
